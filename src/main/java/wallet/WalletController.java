package wallet;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import service.WalletServiceImpl;

@Controller
public class WalletController {
	
	WalletServiceImpl wService; 
	
	public WalletController(){
		wService = new WalletServiceImpl();
	}
	
	@RequestMapping("/")
	@ResponseBody
	public String index() {
		return "API WEB";
	}
	
	@RequestMapping(name="/blockchain/ethereum/infura/client/version", method=RequestMethod.GET)
	@ResponseBody
	public String verifyClientService() {
		return wService.verifyClientVersion();
	}
	
	@RequestMapping(name="/blockchain/ethereum/infura/client/acquire-wallet", method=RequestMethod.GET)
	@ResponseBody
	public String acquireWallet() {
		return wService.acquireWallet();
	}
	
}