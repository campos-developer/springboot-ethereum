package service;

import java.io.File;
import java.io.IOException;

import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.methods.response.Web3ClientVersion;
import org.web3j.protocol.http.HttpService;

public class WalletServiceImpl implements WalletService{
	
	/**
	 * Web3j Documentation
	 * https://web3j.readthedocs.io/en/latest/development.html
	 */
	
	public String verifyClientVersion(){
		try {
			Web3j web3 = Web3j.build(new HttpService("https://rinkeby.infura.io/HTufLP2eqw4HPhy2PSeD"));
			Web3ClientVersion web3ClientVersion = web3.web3ClientVersion().send();
			String clientVersion = web3ClientVersion.getWeb3ClientVersion();
			System.out.println(clientVersion);
			return clientVersion;
		}
		catch(IOException ioe) {
			return ioe.getMessage();
		}
		catch (Exception e) {
			return e.getMessage();
		}
	}
	
	public String acquireWallet(){
		try {
			
			String path = "C:\\Users\\Willie\\Desktop\\Ethereum-Wallet\\";
			File file = new File(path);
			
			String response;
			
			if(file.exists()){
				response =  WalletUtils. generateNewWalletFile("123456abcedf", file, true);
				System.out.println(response);
				return response;
			}else {
				response = "caminho especificado n�o existe";
				System.out.println(response);
				return response;
			}
		}
		catch (Exception e) {
			return e.getMessage();
		}
	}
	
}
