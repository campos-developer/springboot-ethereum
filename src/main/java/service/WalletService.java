package service;

public interface WalletService {
	
	public String verifyClientVersion();
	
	public String acquireWallet();

}
